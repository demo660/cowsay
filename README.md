# cowsay

My personal cowsay docker image

## make build

```sh
make build 
Sending build context to Docker daemon  76.29kB
Step 1/5 : FROM ubuntu:latest
 ---> ba6acccedd29
Step 2/5 : LABEL maintainer="Haggai Philip Zagury <hagzag@tikalk.com>"
 ---> Using cache
 ---> 14c7f5931e59
Step 3/5 : RUN export DEBIAN_FRONTEND=noninteractive &&     apt update &&     apt install -y cowsay &&     rm -rf /var/lib/apt/lists/*
 ---> Using cache
 ---> 9b3bb73cb7ec
Step 4/5 : ENTRYPOINT ["/usr/games/cowsay"]
 ---> Using cache
 ---> a6efe023cb5b
Step 5/5 : CMD ["-f" "tux" "Your docker-cowsay Works !!!"]
 ---> Using cache
 ---> a220a65cefa3
Successfully built a220a65cefa3
Successfully tagged registry.gitlab.com/tikal-external/academy-public/images/cowsay:latest

```

## make push 
```sh
docker login 'registry.gitlab.com'
Authenticating with existing credentials...
Login Succeeded
docker push 'registry.gitlab.com'/tikal-external/academy-public/images/cowsay
Using default tag: latest
The push refers to repository [registry.gitlab.com/tikal-external/academy-public/images/cowsay]
4c17e10428eb: Pushed 
9f54eef41275: Layer already exists 
latest: digest: sha256:7b37cc3e4a5eff1ffa97d07a75b7b8b2f8c440bd6f99935b24519b019e6366fc size: 741
```

## make run

```sh
make run
 _____________________
< AWESOME! local test >
 ---------------------
   \
    \
        .--.
       |o_o |
       |:_/ |
      //   \ \
     (|     | )
    /'\_   _/`\
    \___)=(___/
```